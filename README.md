# chrome-profile-crud



## What?

A straight-forward shell script to create, list, and delete chrome profiles programatically on linux-based operating systems.
The script outputs its responses in json format to facilitate communication between services.

It achieves this by creating a template profile folder which is then copied as a base for the new profiles subsequently created.

## How to use

The script has the following dependencies which should be available in your ``$PATH``

- any implementation of ``docopts`` - [docopts project](https://github.com/docopt)
- any implementation of [jq](https://github.com/jqlang/jq)
- [jj](https://github.com/tidwall/jj)
- [yo](https://github.com/lucasepe/yo)

```
 Usage:
  chrome_profile init
  chrome_profile create [--profile-name=<the-profile-name>]
  chrome_profile list
  chrome_profile delete <profile-name-to-delete> 
  chrome_profile -h | --help | --version

 Examples:
  ./chrome_profile create --profile-name="cool-profile-name"
  ./chrome_profile delete myprofile-123
```



## License
MIT
